# FYP INDIVIDUAL FILES

This repositotory contains all the code that was used in order to develop the Image Classification Algorithm, that was developed as part of the COMP3931 project, and used within the 'Skin Scan' Application


## Index
The following supporting code is stored within this repository:

image-classifier-development-and-analysis-final.ipynb - Showcases the development and evaluation steps taken when developing skin cancer image clasification algorithm.

dataset_creation.ipynb - Notebook that was ran locally to create datasets with image preprocessing steps applied, based on train test and validation dataframes made within data-splitting-and-oversampling.ipynb.

data-splitting-and-oversampling.ipynb - creation of datasets to be used in dataset_creation.ipynb. Applied duplicate isolation and oversampling


## Link to video walkthrough of application
https://youtu.be/Nxc1feTQ9BI


## Links to final kaggle datasets used for evaluation purposes
PAD-UFES-20 with oversampling applied: 
https://www.kaggle.com/datasets/kevinbraszkiewicz/pad-ufes-20-oversampled-v2

PAD-UFES-20 with oversampling and CLAHE applied:
https://www.kaggle.com/datasets/kevinbraszkiewicz/pad-ufes-20-oversampled-clahe-v2


PAD-UFES-20 with Oversampling, CLAHE and SoG applied:
https://www.kaggle.com/datasets/kevinbraszkiewicz/pad-ufes-20-oversampled-clahe-sog-v2

Extension of PAD-UFES-20 with Oversampling, CLAHE and SoG applied, containing only the test dataset cases, split by lesion type:
https://www.kaggle.com/datasets/kevinbraszkiewicz/test-set-by-lesion-type


All the datasets above were made by modifying the distributions and image data of readily available PAD-UFES-20 dataset.
